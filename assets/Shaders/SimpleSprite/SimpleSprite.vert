uniform mat4 g_WorldViewProjectionMatrix;
uniform float g_Time;

attribute vec3 inPosition;
attribute vec2 inTexCoord;
varying vec2 texCoordAni;
 
   // if these are passed as ints, then it doesn't work for some reason
   uniform int m_numTilesU;
   uniform int m_numTilesV;
   uniform int m_Speed;
   uniform int m_selectedTile;

#ifdef FOG
    varying float fog_z;
#endif

#if defined(FOG_SKY)
    varying vec3 I;
    uniform vec3 g_CameraPosition;
    uniform mat4 g_WorldMatrix;
#endif 

void main(){
        gl_Position = g_WorldViewProjectionMatrix * vec4(inPosition, 1.0);
        texCoordAni = inTexCoord;

	int iNumTilesU = int(m_numTilesU);
	int iNumTilesV = int(m_numTilesV);

	int numTilesTotal = iNumTilesU * iNumTilesV;

#if defined(SELECTED_TILE)
        int selectedTile = m_selectedTile;
#else
        int selectedTile = int( g_Time * float(m_Speed) );
#endif

        float currentXTile = mod(selectedTile , iNumTilesU);
        float currentYTile = mod(floor(float(selectedTile) / float(iNumTilesU)), float(iNumTilesV)) + 1.0;
        
        float col = (inTexCoord.x + currentXTile) / float(iNumTilesU);
        float row = (-inTexCoord.y + currentYTile) / float(iNumTilesV);

        texCoordAni.x = col;
        texCoordAni.y = 1.0 - row;

#if defined(FOG_SKY)
       vec3 worldPos = (g_WorldMatrix * pos).xyz;
       I = normalize( g_CameraPosition -  worldPos  ).xyz;
#endif

#ifdef FOG
    fog_z = gl_Position.z;
#endif

}

