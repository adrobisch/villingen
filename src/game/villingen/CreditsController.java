/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game.villingen;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;

/**
 *
 * @author andreas
 */
public class CreditsController implements ScreenController {
    private Nifty nifty;
    private Screen screen;

    public void bind(Nifty nifty, Screen screen) {
        this.nifty = nifty;
        this.screen = screen;
    }

    public void onStartScreen() {
    }

    public void onEndScreen() {
    }
    
    public void back() {
        nifty.gotoScreen("menu");
    }
    
}
