/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game.villingen;

import com.jme3.asset.AssetManager;
import com.jme3.audio.AudioNode;
import com.jme3.bullet.PhysicsSpace;
import com.jme3.bullet.collision.PhysicsCollisionEvent;
import com.jme3.bullet.collision.PhysicsCollisionListener;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.bullet.objects.PhysicsGhostObject;
import com.jme3.effect.ParticleEmitter;
import com.jme3.effect.ParticleMesh.Type;
import com.jme3.effect.shapes.EmitterSphereShape;
import com.jme3.export.JmeExporter;
import com.jme3.export.JmeImporter;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import game.villingen.VillingenListener.Event;
import java.io.IOException;
import java.util.Iterator;

/**
 *
 * @author normenhansen
 */
public class PlaneControl extends RigidBodyControl implements PhysicsCollisionListener {

    private float explosionRadius = 10;
    private PhysicsGhostObject ghostObject;
    private Vector3f vector = new Vector3f();
    private Vector3f vector2 = new Vector3f();
    private float forceFactor = 1;
    private ParticleEmitter effect;
    private float fxTime = 0.5f;
    private float maxTime = 4f;
    private float curTime = -1.0f;
    private float timer;
    private final VillingenNode node;

    public PlaneControl(AssetManager manager, VillingenNode node, CollisionShape shape, float mass) {
        super(shape, mass);
        
        this.node = node;
        prepareEffect(manager);
        
        setKinematic(true);
    }

    public void setPhysicsSpace(PhysicsSpace space) {
        super.setPhysicsSpace(space);
        if (space != null) {
            space.addCollisionListener(this);
        }
    }

    private void prepareEffect(AssetManager assetManager) {
        int COUNT_FACTOR = 1;
        float COUNT_FACTOR_F = 1f;
        effect = new ParticleEmitter("Flame", Type.Triangle, 32 * COUNT_FACTOR);
        effect.setSelectRandomImage(true);
        effect.setStartColor(new ColorRGBA(1f, 0.4f, 0.05f, (float) (1f / COUNT_FACTOR_F)));
        effect.setEndColor(new ColorRGBA(.4f, .22f, .12f, 0f));
        effect.setStartSize(0.3f);
        effect.setEndSize(0.5f);
        effect.setShape(new EmitterSphereShape(Vector3f.ZERO, 1f));
        effect.setParticlesPerSec(0);
        effect.setGravity(0, -2f, 0);
        effect.setLowLife(.7f);
        effect.setHighLife(.9f);
        effect.setInitialVelocity(new Vector3f(0, 0.1f, 0));
        effect.setVelocityVariation(1f);
        effect.setImagesX(2);
        effect.setImagesY(2);
        
        Material mat = new Material(assetManager, "Common/MatDefs/Misc/Particle.j3md");
        mat.setTexture("Texture", assetManager.loadTexture("Textures/Effects/Explosion/flame.png"));
        effect.setMaterial(mat);
    }
    
    private Node getBuildingParent(Spatial toSearch, String searchString) {
        
        Node currentParent = toSearch.getParent();
        
        while (currentParent != null) {
//            System.out.println(""+currentParent.getName());
            if (currentParent.getName().toLowerCase().contains(searchString)) {
                return currentParent;
            }
            currentParent = currentParent.getParent();
        }
        
        return null;
    }

    public void collision(PhysicsCollisionEvent event) {
        if (! (event.getNodeA() instanceof VillingenNode) && ! (event.getNodeB() instanceof VillingenNode)) {
            return;
        }
        
        if (event.getNodeB().getName().equals("Floor")) {
            
            VillingenNode nodeA = (VillingenNode) event.getNodeA();
            nodeA.kill();
            nodeA.getControl(PlaneControl.class).explosionEffect(nodeA);
            
        }else if (event.getNodeA().getName().equals("Floor")) {
            
            VillingenNode nodeB = (VillingenNode) event.getNodeB();
            nodeB.kill();
            nodeB.getControl(PlaneControl.class).explosionEffect(nodeB);
        }
        else if (getBuildingParent(event.getNodeA(), "building") != null || 
                 getBuildingParent(event.getNodeB(), "building") != null) {
            Node parent = null;
            
            if(getBuildingParent(event.getNodeA(), "building") != null) {
               parent = (Node) event.getNodeA().getParent();
               killPlane(event.getNodeB(), Event.BUILIDING_COLLISION);
               explosionSound(parent);
            }
            else {
               parent = (Node) event.getNodeB().getParent();
               killPlane(event.getNodeA(), Event.BUILIDING_COLLISION);
               explosionSound(parent);
            }
            
            for (Iterator<Spatial> it = parent.getChildren().iterator(); it.hasNext();) {    
               Spatial child = it.next();
               if(child.getControl(RigidBodyControl.class)!= null)
                   child.getControl(RigidBodyControl.class).setKinematic(false);
            }
        }else {
            killPlane(event.getNodeA(), Event.PLANE_COLLISION);
            killPlane(event.getNodeB(), Event.PLANE_COLLISION);
        }

    }
    
    private void killPlane(final Spatial planeNode, Event event) {
        VillingenNode villingenNode = (VillingenNode) planeNode;
        
        float linearMulti = 4.0f;
        
        PlaneControl control = villingenNode.getControl(PlaneControl.class);
        if (control != null) {
            control.setLinearVelocity(villingenNode.getTargetDir().setZ(-3).normalize().mult(linearMulti));
            control.setKinematic(false);
            control.explosionEffect(villingenNode);
            villingenNode.kill(event);
        }
    }
    
    public void explosionEffect (VillingenNode villingenNode) {
        effect.setLocalTranslation(spatial.getLocalTranslation());
        if (spatial.getParent() != null) {
            spatial.getParent().attachChild(effect);
        }
        effect.emitAllParticles();
        explosionSound(villingenNode);
    }
    
    public void explosionSound (Node node) {
        AudioNode explosion = (AudioNode) node.getChild("explosion");
        explosion.play();
    }
    
    @Override
    public void update(float tpf) {
        super.update(tpf);
        
//        if(enabled){
//            timer+=tpf;
//            if(timer>maxTime){
//                if(spatial.getParent()!=null){
//                    space.removeCollisionListener(this);
//                    space.remove(this);
//                    spatial.removeFromParent();
//                }
//            }
//        }
//        if (enabled && curTime >= 0) {
//            curTime += tpf;
//            if (curTime > fxTime) {
//                curTime = -1;
//                effect.removeFromParent();
//            }
//        }
    }

    @Override
    public void read(JmeImporter im) throws IOException {
        throw new UnsupportedOperationException("Reading not supported.");
    }

    @Override
    public void write(JmeExporter ex) throws IOException {
        throw new UnsupportedOperationException("Saving not supported.");
    }
    
}
