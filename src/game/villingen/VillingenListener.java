/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game.villingen;

/**
 *
 * @author andreas
 */
interface VillingenListener {
    
    public enum Event {
        NODE_KILL,
        PLANE_COLLISION,
        FLOOR_COLLISION,
        BUILIDING_COLLISION
    }
    
    public void notify(Event event, VillingenNode node);
}
