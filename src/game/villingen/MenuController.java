/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game.villingen;

import com.jme3.app.Application;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;

/**
 *
 * @author andreas
 */
public class MenuController extends AbstractAppState implements ScreenController {
 
  private Nifty nifty;
  private Screen screen;
  private Main app;
  private AppStateManager stateManager;
 
  /** Nifty GUI ScreenControl methods */ 
 
  public void bind(Nifty nifty, Screen screen) {
    this.nifty = nifty;
    this.screen = screen;
  }
 
  public void quit() {
      System.out.println("QUIT");
      app.stop();
  }
  
  public void credits() {
      nifty.gotoScreen("credits");
  }
  
  public void start() {
      app.startGame();
  }

  public void onStartScreen() {
  }

  public void onEndScreen() {
  }

    @Override
  public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);
        this.stateManager = stateManager;
        this.app = (Main) app;
  }
  
  
}
