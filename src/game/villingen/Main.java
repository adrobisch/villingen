package game.villingen;

import aurelienribon.tweenengine.BaseTween;
import banana.seed.tween.MaterialAccessor;
import banana.seed.tween.SpatialAccessor;
import banana.seed.tween.MaterialAccessorParameter;
import banana.seed.font.TTFFontLoader;
import banana.seed.font.FontLocator;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;
import aurelienribon.tweenengine.TweenManager;
import com.jme3.app.SimpleApplication;
import com.jme3.audio.AudioNode;
import com.jme3.bounding.BoundingBox;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.PhysicsSpace;
import com.jme3.bullet.collision.shapes.BoxCollisionShape;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.bullet.util.CollisionShapeFactory;
import com.jme3.collision.CollisionResult;
import com.jme3.collision.CollisionResults;
import com.jme3.cursors.plugins.JmeCursor;
import com.jme3.font.BitmapFont;
import com.jme3.font.BitmapText;
import com.jme3.font.Rectangle;
import com.jme3.input.MouseInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.controls.MouseButtonTrigger;
import com.jme3.light.AmbientLight;
import com.jme3.light.DirectionalLight;
import com.jme3.light.PointLight;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Plane;
import com.jme3.math.Ray;
import com.jme3.math.Vector3f;
import com.jme3.niftygui.NiftyJmeDisplay;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.queue.RenderQueue;
import com.jme3.scene.Geometry;
import com.jme3.scene.Mesh;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.debug.Arrow;
import com.jme3.scene.shape.Box;
import com.jme3.shadow.PssmShadowRenderer;
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.input.keyboard.KeyboardInputEvent;
import game.villingen.VillingenListener.Event;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * test
 *
 * @author normenhansen
 */
public class Main extends SimpleApplication {

    public static final String CLICKED = "Clicked";
    public static final String ESCAPE = "Escape";
    
    public static final int PLANE_LEFT_BOUND = -18;
    public static final int PLANE_BOTTOM_BOUND = -18;
    public static final int PLANE_RIGHT_BOUND = 18;
    public static final int PLANE_TOP_BOUND = 18;
    
    public static final int LEFT_TARGET_BOUND = -10;
    public static final int RIGHT_TARGET_BOUND = 10;
    public static final int TOP_TARGET_BOUND = 10;
    public static final int BOTTOM_TARGET_BOUND = -10;
    
    public static final int KILL_RADIUS = 20;
    
    private TweenManager tweenManager;
    private BitmapFont fnt;
    private ArrowWrapper selectArrow;
    private VillingenNode hoveredPlane;
    private VillingenNode draggedPlane;
    private BetterArrow dragArrow;
    private static Box floor;
    List<VillingenNode> airPlanes = new ArrayList<VillingenNode>();
    private int planeMaxCount = 3;
    private final int PLANES_SPAWN_CHANCE = 50;
    private BulletAppState bulletAppState;
    private float airPlaneAltitude = 12.0f;
    private BitmapText pointText;
    private int points = 0;

    private boolean debugMode = false;
    
    public static void main(String[] args) {
        Main app = new Main();
        app.start();
    }
    private PssmShadowRenderer bsr;
    private Nifty nifty;
    private BitmapText timeText;
    private int timeLeft = 0;
    private boolean inProgress = false;
    private Tween spawnTween;
    private BitmapText scoreTxt;
    private Node floor_node;
    private Node buildingsNode;
    private Node hudNode = new Node("Game Hud");
    private Tween timeleftTween;
    
    private void add_floor()
    {
        floor_node = (Node)assetManager.loadModel("Models/streets.j3o");
        floor_node.setName("Floor");
        floor_node.move(0,0, 0.f);
        floor_node.rotate((float)Math.toRadians(90), 0, 0);
        
        RigidBodyControl bodyControl = new RigidBodyControl(new BoxCollisionShape(new Vector3f(100.0f, 0.01f, 100.0f)), 0);
        
        floor_node.addControl(bodyControl);
        
        floor_node.setShadowMode(RenderQueue.ShadowMode.Receive);

        rootNode.attachChild(floor_node);
        this.getPhysicsSpace().add(floor_node);           
    }
    
    private void add_building(Node parent, Vector3f origin, String type)
    {
        
        Node building_node = (Node) assetManager.loadModel(type);
        building_node.setName("Building");
        
        building_node.move(origin);
        building_node.setMaterial(unshadedMaterial(ColorRGBA.Gray));

        for ( int i = 0; i<building_node.getQuantity() ;i++ )
        {
            building_node.getChild(i).addControl(new RigidBodyControl(1.5f));
            building_node.getChild(i).getControl(RigidBodyControl.class).setFriction(0.6f);
            building_node.getChild(i).getControl(RigidBodyControl.class).setKinematic(true);    
        }
        
        building_node.rotate((float)Math.toRadians(90), 0, 0);
        building_node.setShadowMode(RenderQueue.ShadowMode.Cast);
        addExplosionAudioNode(building_node, "Sounds/explosion_building-Mike_Koenig.ogg");
        
        parent.attachChild(building_node);

        for ( int i = 0; i<building_node.getQuantity() ;i++ ) {
            Node child = (Node) building_node.getChild(i);
            if (! (child instanceof AudioNode)) {
                this.getPhysicsSpace().add(child);  
            }
        }
        
        
                
    }
    
    public void removeBuildingFromPhysics() {
        for (Spatial building : buildingsNode.getChildren()) {
            Node buildingNode = (Node) building;
            for ( int i = 0; i<buildingNode.getQuantity() ;i++ ) {
                Node buildingChild = (Node) buildingNode.getChild(i);
                if (! (buildingChild instanceof AudioNode)) {
                    this.getPhysicsSpace().remove(buildingChild); 
                }
            }
        }
    }
    

    public void initTweenEngine() {
        tweenManager = new TweenManager();

        Tween.registerAccessor(MaterialAccessorParameter.class, new MaterialAccessor());
        Tween.registerAccessor(Spatial.class, new SpatialAccessor());
        Tween.setCombinedAttributesLimit(4);
    }
    
    public void startGame() {
        nifty.removeScreen("menu");
        points = 0;
        timeLeft = 121;
        planeMaxCount = 3;
        inProgress = true;
        
        removeBuildingFromPhysics();
        buildingsNode.detachAllChildren();
        
        initBuildings();
        initHud();
        
        for (VillingenNode node : airPlanes.toArray(new VillingenNode[] {})) {
            node.kill();
        }
        
        if (timeleftTween != null) {
            timeleftTween.free();
        }
        
        timeleftTween = Tween.call(new TweenCallback() {

                public void onEvent(int type, BaseTween<?> source) {
                    timeLeft--;
                    if (timeLeft % 10 == 0) {
                        planeMaxCount++;
                    }
                    if (timeLeft == 0) {
                        showHighScore();
                    }
                }
         }).repeat(timeLeft-1, 1.0f).start(tweenManager);
        
        spawnTween.resume();
    }
    
    public void showHighScore() {
        inProgress = false;
        spawnTween.pause();
        
        scoreTxt = new BitmapText(guiFont, false);
        scoreTxt.setBox(new Rectangle(-3, 0, 6, 0));
        scoreTxt.setLocalTranslation(cam.getLocation().add(cam.getDirection().normalize().mult(6).add(new Vector3f(2,-2,0))));
        scoreTxt.setQueueBucket(RenderQueue.Bucket.Transparent);
        scoreTxt.setSize(0.33f);
        scoreTxt.setColor(ColorRGBA.Orange);
        scoreTxt.setText("Your Score: "+points+" !");
        
        rootNode.attachChild(scoreTxt);
        
        nifty.fromXml("Interface/startMenu.xml", "menu");
    }

    public void initFonts() {
        TTFFontLoader.useAntialiasing = false;

        assetManager.registerLocator("", FontLocator.class);
        assetManager.registerLoader(TTFFontLoader.class.getName(), "ttf");

        this.fnt = assetManager.loadFont("font(48,0x000000):Fonts/amigaforever2.ttf");
    }

    public void log(String logText) {
        System.out.println(logText);
    }

    public void initAirPlaneScene() {
        viewPort.setBackgroundColor(ColorRGBA.Black);

        /**
         * Load a model. Uses model and texture from jme3-teaddst-data library!
         */
        /**
         * A white ambient light source.
         */
        initLightAndShadow();

        flyCam.setDragToRotate(true);
        flyCam.setRotationSpeed(0.0f);

        flyCam.setZoomSpeed(3.0f);
        flyCam.setMoveSpeed(3.0f);
        flyCam.setEnabled(true);

        inputManager.setCursorVisible(true);
        inputManager.setMouseCursor((JmeCursor) assetManager.loadAsset("Interface/hand_cursor.cur"));

        cam.setLocation(new Vector3f(0, 0.0f, 45.0f));
        cam.lookAt(Vector3f.ZERO, Vector3f.UNIT_Y);

        spawnPlanes();

        selectArrow = createArrow(rootNode, new Vector3f(), new Vector3f());
        
        initInput();
        initNiftyUi();
        initCity();
        
        //        initPhysicsScene();
    }

    public ArrowWrapper createArrow(Node parent, Vector3f startPos, Vector3f endPos) {
        Arrow arrow = new Arrow(endPos);
        arrow.setLineWidth(2);
        Geometry mark = new Geometry("mark", arrow);
        mark.setLocalTranslation(startPos);

        Material mark_mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        mark_mat.setColor("Color", ColorRGBA.Red);
        mark.setMaterial(mark_mat);

        parent.attachChild(mark);
        return new ArrowWrapper(arrow, mark);
    }

    private void attachCoordinateAxes(Vector3f pos) {
        Arrow arrow = new Arrow(Vector3f.UNIT_X);
        arrow.setLineWidth(4); // make arrow thicker
        putShape(arrow, ColorRGBA.Red).setLocalTranslation(pos);

        arrow = new Arrow(Vector3f.UNIT_Y);
        arrow.setLineWidth(4); // make arrow thicker
        putShape(arrow, ColorRGBA.Green).setLocalTranslation(pos);

        arrow = new Arrow(Vector3f.UNIT_Z);
        arrow.setLineWidth(4); // make arrow thicker
        putShape(arrow, ColorRGBA.Blue).setLocalTranslation(pos);
    }

    private Geometry putShape(Mesh shape, ColorRGBA color) {
        Geometry g = new Geometry("coordinate axis", shape);
        Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        mat.getAdditionalRenderState().setWireframe(true);
        mat.setColor("Color", color);
        g.setMaterial(mat);
        rootNode.attachChild(g);
        return g;
    }

    public void attachBoundingBox(Node parent, Spatial spatial, ColorRGBA color) {
        float xExt = ((BoundingBox) spatial.getWorldBound()).getXExtent();
        float yExt = ((BoundingBox) spatial.getWorldBound()).getYExtent();
        float zExt = ((BoundingBox) spatial.getWorldBound()).getZExtent();

        Geometry g = new Geometry("bounding cube", new Box(xExt, yExt, zExt));
        Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        mat.getAdditionalRenderState().setWireframe(false);
        g.setCullHint(Spatial.CullHint.Always);

        mat.setColor("Color", color);
        g.setMaterial(mat);
        g.setLocalTranslation(spatial.getLocalTranslation());
        parent.attachChild(g);
    }

    @Override
    public void simpleInitApp() {
        initTweenEngine();
        initFonts();
        initPhysics();

        initAirPlaneScene();
        initDebug();
        addMusicNode();
    }

    @Override
    public void simpleUpdate(float tpf) {
        tweenManager.update(tpf);
        listener.setLocation(cam.getLocation());
        listener.setRotation(cam.getRotation());

        checkMouseHovered();
        updateDragMove();

        updateGui();

        if (debugMode) {
            updateDebug();
        }
    }

    @Override
    public void update() {
        super.update();
    }

    @Override
    public void simpleRender(RenderManager rm) {
    }

    private void initInput() {
        inputManager.clearMappings();
        
        inputManager.addMapping(CLICKED, new MouseButtonTrigger(MouseInput.BUTTON_LEFT));
        inputManager.addMapping(ESCAPE, new KeyTrigger(KeyboardInputEvent.KEY_ESCAPE));
        
        inputManager.addListener(selectActionListener, new String[]{CLICKED});
        
        inputManager.addListener(new ActionListener() {

            public void onAction(String name, boolean isPressed, float tpf) {
                nifty.fromXml("Interface/startMenu.xml", "menu");
            }
            
        }, new String[]{ESCAPE});
    }
    private ActionListener selectActionListener = new ActionListener() {
        public void onAction(String name, boolean clicked, float tpf) {
            if (name.equals(CLICKED) && clicked) {
                if (hoveredPlane != null) {
                    hoveredPlane.handleSelect();
                    handleDragStart(hoveredPlane);
                }
            } else if (name.equals(CLICKED) && !clicked) {
                handleDragEnd(draggedPlane);
            }
        }
    };

    private void handleDragEnd(VillingenNode draggedPlane) {

        if (draggedPlane != null) {
            draggedPlane.tweenTo(dragArrow.getEndPos());
        }

        this.draggedPlane = null;
        if (this.dragArrow != null) {
            this.dragArrow.getGeometry().removeFromParent();
            this.dragArrow = null;
        }
    }

    private void handleDragStart(VillingenNode hoveredPlane) {
        this.draggedPlane = hoveredPlane;
        this.dragArrow = new BetterArrow(rootNode, unshadedMaterial(), hoveredPlane.getLocalTranslation(), getMousePosition().setZ(hoveredPlane.getLocalTranslation().getZ()));
    }

    private void updateDragMove() {
        if (dragArrow != null) {
            Vector3f planeTranslation = draggedPlane.getLocalTranslation();
            log("planeTrans X" + planeTranslation.getX());

            log("planeTrans spatial x" + draggedPlane.getSpatial().getLocalTranslation().getX());

            Vector3f intersection = new Vector3f();
            getMouseDirectionRay().intersectsWherePlane(new Plane(Vector3f.UNIT_Z, planeTranslation.getZ()), intersection);

            dragArrow.setStart(planeTranslation);
            dragArrow.setEnd(intersection);

            log("dragArrow X" + dragArrow.getGeometry().getLocalTranslation().getX());
        }
    }

    private Vector3f getMousePosition(float zPos) {
        return cam.getWorldCoordinates(inputManager.getCursorPosition(), zPos);
    }

    private Vector3f getMousePosition() {
        return getMousePosition(0.0f);
    }

    private void checkMouseHovered() {
        if (!inProgress) {
            return;
        }
        
        Vector3f origin = getMousePosition();
        Vector3f direction = getNormalizedMouseDirection(origin);

        Ray ray = new Ray(origin, direction);

        CollisionResults results = new CollisionResults();
        rootNode.collideWith(ray, results);

        hoveredPlane = null;

        for (CollisionResult collision : results) {
            
            VillingenNode villingenNode = VillingenNode.getVillingenNode(collision.getGeometry());
            if (villingenNode != null) {
                hoveredPlane = villingenNode;
            }
        }
    }

    protected Material unshadedMaterial(ColorRGBA color) {
        Material mat = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md");
//        mat.setColor("Color", color);
        return mat;
    }

    protected Material unshadedMaterial() {
        return unshadedMaterial(ColorRGBA.Blue);
    }

    private Vector3f getNormalizedMouseDirection(Vector3f origin) {
        Vector3f direction = getMousePosition(0.3f);
        direction.subtractLocal(origin).normalizeLocal();
        return direction;
    }

    private Ray getMouseDirectionRay() {
        Vector3f origin = getMousePosition();
        Vector3f direction = getNormalizedMouseDirection(origin);

        return new Ray(origin, direction);
    }

    public void spawnPlanes() {
        spawnTween = Tween.call(new TweenCallback() {
            public void onEvent(int type, BaseTween<?> source) {
                if (getRandomInInterval(0, 100) > PLANES_SPAWN_CHANCE && airPlanes.size() < planeMaxCount) {
                    airPlanes.add(spawnPlane());
                }
            }
        }).repeat(Tween.INFINITY, 0.5f).start(tweenManager);
    }

    public VillingenNode spawnPlane() {
        VillingenNode planeNode = new VillingenNode(assetManager.loadModel("Models/a10plane.j3o"), tweenManager);
        planeNode.setShadowMode(RenderQueue.ShadowMode.CastAndReceive);
        planeNode.getSpatial().scale(2.0f);

        rootNode.attachChild(planeNode);
        attachBoundingBox(planeNode, planeNode.getSpatial(), ColorRGBA.Blue);
        
        int startSide = getRandomInInterval(0, 3);
        
        Vector3f spawnLocation = null;
        
        switch (startSide) {
            case 1:
                // start right
                spawnLocation = new Vector3f(PLANE_RIGHT_BOUND, getRandomInInterval(PLANE_TOP_BOUND, PLANE_BOTTOM_BOUND), airPlaneAltitude);
                break;
            case 2:
                // start top
                spawnLocation = new Vector3f(getRandomInInterval(PLANE_LEFT_BOUND, PLANE_RIGHT_BOUND), PLANE_TOP_BOUND, airPlaneAltitude);
                break;
            case 3:
                // start bottom
                spawnLocation = new Vector3f(getRandomInInterval(PLANE_LEFT_BOUND, PLANE_RIGHT_BOUND), PLANE_BOTTOM_BOUND, airPlaneAltitude);
                break;
            
            default: 
                // start left
                spawnLocation = new Vector3f(PLANE_LEFT_BOUND, getRandomInInterval(PLANE_TOP_BOUND, PLANE_BOTTOM_BOUND), airPlaneAltitude);
        }
        
        planeNode.setLocalTranslation(spawnLocation);

        planeNode.getLocalRotation().lookAt(Vector3f.UNIT_X, Vector3f.UNIT_Z);
        planeNode.tweenTo(new Vector3f(getRandomInInterval(LEFT_TARGET_BOUND, RIGHT_TARGET_BOUND), getRandomInInterval(TOP_TARGET_BOUND, BOTTOM_TARGET_BOUND), airPlaneAltitude));

        planeNode.addListener(airPlaneKillListener);

        addPhysics(planeNode);
        addExplosionAudioNode(planeNode, "Sounds/explosion.ogg");
        
        return planeNode;
    }
    
    private void addExplosionAudioNode(Node node, String path) {
        AudioNode explosionSound = new AudioNode(assetManager, path, false);
        
        explosionSound.setName("explosion");
        explosionSound.setPositional(true);
        explosionSound.setLocalTranslation(100.0f, 0.0f, 0.0f);
        explosionSound.setReverbEnabled(false);
        explosionSound.setPitch(new Random().nextFloat()+0.7f);

        node.attachChild(explosionSound);
    }
    
    private void addMusicNode() {
        AudioNode bgMusic = new AudioNode(assetManager, "Sounds/gruen005_gras_03-gras_-_bluetenstaub.ogg", true);
        bgMusic.setLooping(true);
        bgMusic.play();

        rootNode.attachChild(bgMusic);
    }


    private void addPhysics(VillingenNode node) {
        CollisionShape nodeCollisionShape = CollisionShapeFactory.createDynamicMeshShape(node.getSpatial());

        RigidBodyControl bodyControl = new PlaneControl(assetManager, node, nodeCollisionShape, 1);

        node.addControl(bodyControl);
        getPhysicsSpace().add(bodyControl);
    }
    private VillingenListener airPlaneKillListener = new VillingenListener() {
        public void notify(Event event, VillingenNode node) {
            if (event == Event.NODE_KILL) {
                airPlanes.remove(node);
                getPhysicsSpace().remove(node.getControl(PlaneControl.class));

            }
            if (event == Event.PLANE_COLLISION) {
                points++;
            }
            
            if (event == Event.BUILIDING_COLLISION) {
                points+=100;
            }
            
        }
    };

    private int getRandomInInterval(int start, int end) {
        int random = new Random().nextInt(Math.abs(end) + Math.abs(start) + 1) - Math.abs(start);
        System.out.println("random" + random);
        return random;
    }

    private PhysicsSpace getPhysicsSpace() {
        return bulletAppState.getPhysicsSpace();
    }

    private void initPhysics() {
        bulletAppState = new BulletAppState();
        bulletAppState.setThreadingType(BulletAppState.ThreadingType.PARALLEL);
        stateManager.attach(bulletAppState);

        bulletAppState.getPhysicsSpace().setGravity(Vector3f.UNIT_Z.mult(-9.81f));
    }

    private void initDebug() {
        if (debugMode) {
            attachCoordinateAxes(Vector3f.ZERO);
            bulletAppState.getPhysicsSpace().enableDebug(assetManager);
            setDisplayStatView(true);
            setDisplayFps(true);
        }else {
            setDisplayStatView(false);
            setDisplayFps(false);
        }
    }

    private void updateDebug() {
        Vector3f origin = getMousePosition();
        Vector3f direction = getNormalizedMouseDirection(origin);

        if (selectArrow != null) {
            selectArrow.setEnd(direction);
            selectArrow.setStart(origin);
        }
    }

    private void initPhysicsScene() {
        floor = new Box(new Vector3f(0, 0, 0), 12.0f, 12.0f, 1.0f);

        Geometry floor_geo = new Geometry("Floor", floor);
        floor_geo.setMaterial(unshadedMaterial(ColorRGBA.Gray));
        floor_geo.setLocalTranslation(0, 0.0f, 0.0f);
        floor_geo.setShadowMode(RenderQueue.ShadowMode.CastAndReceive);
        this.rootNode.attachChild(floor_geo);

        /* Make the floor physical with mass 0.0f! */
        RigidBodyControl floor_phy = new RigidBodyControl(0.0f);
        floor_geo.addControl(floor_phy);

        bulletAppState.getPhysicsSpace().add(floor_phy);
    }

    private void initHud () {
        
        if (scoreTxt != null) {
            scoreTxt.removeFromParent();
        }
        
        if (hudNode.getParent() == null) {
            guiNode.attachChild(hudNode);
        }
        
        hudNode.detachAllChildren();
        
        pointText = new BitmapText(guiFont, false);
        pointText.setSize(guiFont.getCharSet().getRenderedSize());      // font size
        pointText.setColor(ColorRGBA.Orange);                             // font color
        pointText.setText("");             // the text
        pointText.setLocalTranslation(5, settings.getHeight() - 5, 0); // position

        timeText = new BitmapText(guiFont, false);
        timeText.setSize(guiFont.getCharSet().getRenderedSize());      // font size
        timeText.setColor(ColorRGBA.Orange);                             // font color
        timeText.setText("");             // the text
        timeText.setLocalTranslation(5, settings.getHeight() - pointText.getLineHeight() -5, 0); // position
        
        hudNode.attachChild(pointText);
        hudNode.attachChild(timeText);
    }

    private void updateGui() {
        if (pointText != null) {
            pointText.setText("Points: " + points);
            timeText.setText("Time Left: " + timeLeft);
        }
    }

    private void initLightAndShadow () {
        bsr = new PssmShadowRenderer(assetManager, 1024, 3);
        bsr.setShadowIntensity(0.2f);
        bsr.setFilterMode(PssmShadowRenderer.FilterMode.Dither);
        
        Vector3f lightDirection = new Vector3f(1.02f,0.52f, -1.0f).normalizeLocal();
        bsr.setDirection(lightDirection); // light direction
        viewPort.addProcessor(bsr);

        addSunLight(lightDirection);
        
//        PointLight pointLight = new PointLight();
//        pointLight.setPosition(new Vector3f(0,30,0));
//        rootNode.addLight(pointLight);
    }

    private void initNiftyUi() {
        NiftyJmeDisplay niftyDisplay = new NiftyJmeDisplay(assetManager,
                                                          inputManager,
                                                          audioRenderer,
                                                          guiViewPort);
        nifty = niftyDisplay.getNifty();
        
        MenuController menuController = new MenuController();
        menuController.initialize(getStateManager(), this);
        
        nifty.registerScreenController(menuController);
        nifty.registerScreenController(new CreditsController());
        
        nifty.fromXml("Interface/startMenu.xml", "menu");

        // attach the nifty display to the gui view port as a processor
        guiViewPort.addProcessor(niftyDisplay);
    }

    private void initCity() {
        add_floor();
        buildingsNode = new Node("nodeB");
        rootNode.attachChild(buildingsNode);
        
        initBuildings();
    }
    
    public void initBuildings(){
//        add_building(buildingsNode, new Vector3f(0,0,0.0f),"Models/buildings/building_house1.j3o");
//        add_building(buildingsNode,new Vector3f(10f,0,0f),"Models/buildings/building_house2.j3o");
//        add_building(buildingsNode, new Vector3f(-10f,0,0f),"Models/buildings/building_house3.j3o");
//        add_building(buildingsNode, new Vector3f(-10f, 10.0f, 0),"Models/buildings/building_berlin_tower.j3o");
//        add_building(buildingsNode, new Vector3f(10f,10,0),"Models/buildings/building_house6.j3o");
        
        add_building(buildingsNode, new Vector3f( -10f,   10f, 0.0f),"Models/buildings/building_berlin_tower.j3o");
        add_building(buildingsNode, new Vector3f(   2f,  2.3f, 0.0f),"Models/buildings/building_house1.j3o");
        add_building(buildingsNode, new Vector3f(   8f,    5f, 0.0f),"Models/buildings/building_house3.j3o");
        add_building(buildingsNode, new Vector3f(  -4f,    6f, 0.0f),"Models/buildings/building_house1.j3o");
        add_building(buildingsNode, new Vector3f(  -6f,   -3f, 0.0f),"Models/buildings/building_house1.j3o");
        add_building(buildingsNode, new Vector3f( -15f, -0.5f, 0.0f),"Models/buildings/building_house3.j3o");
        add_building(buildingsNode, new Vector3f(13.5f,   -9f, 0.0f),"Models/buildings/building_house6.j3o");
        add_building(buildingsNode, new Vector3f( 10.f,   16f, 0.0f),"Models/buildings/building_gate.j3o");
        add_building(buildingsNode, new Vector3f(-10.f, -11f,  0f),"Models/buildings/building_tower.j3o"); 
    }

    private void addSunLight(Vector3f lightDirection) {
        DirectionalLight sun = new DirectionalLight();
        sun.setDirection(lightDirection);
        
        ColorRGBA sunColor = new ColorRGBA();
        sunColor.fromIntRGBA(0xFFF4E5FF);
        sun.setColor(sunColor);

        rootNode.addLight(sun);
    }

}
