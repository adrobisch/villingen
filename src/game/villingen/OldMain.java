/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game.villingen;

import aurelienribon.tweenengine.Timeline;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenEquations;
import banana.seed.sprite.Sprite;
import banana.seed.sprite.SpriteAnimation;
import banana.seed.sprite.SpriteFactory;
import banana.seed.tween.BitmapTextCallback;
import banana.seed.tween.SpatialAccessor;
import com.jme3.font.BitmapText;
import com.jme3.font.Rectangle;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.renderer.queue.RenderQueue;
import com.jme3.scene.Geometry;
import com.jme3.scene.control.BillboardControl;
import com.jme3.scene.shape.Box;
import com.jme3.texture.Texture;
import com.jme3.util.TangentBinormalGenerator;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author andreas
 */
public class OldMain {
//       public void initTweenScene() {
//        String txtB = "";
////        BitmapFont fnt = assetManager.loadFont("Interface/Fonts/Default.fnt");
//        final BitmapText txt = new BitmapText(fnt, false);
//        txt.setBox(new Rectangle(-3, 0, 6, 0));
//        txt.setLocalTranslation(-20, 0, 3);
//        txt.setQueueBucket(RenderQueue.Bucket.Transparent);
//        txt.setSize(0.33f);
//        txt.setText(txtB);
//        rootNode.attachChild(txt);
//
//        Tween.to(txt, SpatialAccessor.LOCAL_TRANSLATION, 6.0f)
//                .target(0.0f, 0.0f, 3.0f).start(tweenManager);
//
//        BillboardControl control = new BillboardControl();
////        txt.addControl(control);
//        Timeline.createSequence()
//                .push(
//                Tween.call(new BitmapTextCallback(txt, "Hey du!")).delay(0.0f))
//                .push(
//                Tween.call(new BitmapTextCallback(txt, "Ja, du!")).delay(3.0f))
//                .push(
//                Tween.call(new BitmapTextCallback(txt, "Shut up and write a game!")).delay(3.0f))
//                .start(tweenManager);
//
//        Box b = new Box(Vector3f.ZERO, 0.5f, 0.5f, 0.5f);
//        Geometry geom = new Geometry("Box", b);
//        geom.setQueueBucket(RenderQueue.Bucket.Transparent);
//        //geom.addControl(new BillboardControl());
//
//        Box b2 = new Box(Vector3f.ZERO, 1, 1, 0);
//        Geometry geom2 = new Geometry("Box", b2);
//
//        viewPort.setBackgroundColor(ColorRGBA.White);
//
//        Timeline.createSequence()
//                .push(Tween.to(geom, SpatialAccessor.LOCAL_ROTATION, 3.0f)
//                .target(0.0f, 360.0f, 0.0f))
//                .push(Tween.to(geom, SpatialAccessor.LOCAL_TRANSLATION, 1.0f)
//                .target(10.0f, -2.0f, -10.0f)
//                .ease(TweenEquations.easeOutBounce))
//                .push(Tween.to(geom, SpatialAccessor.LOCAL_SCALE, 1.0f).target(10, 6, 5))
//                .repeatYoyo(-1, 0)
//                .start(tweenManager);
//
//        Material sprite2 = assetManager.loadMaterial("Materials/SimpleSprite/SimpleSprite_2.j3m");
//
////        Tween.to(new MaterialAccessorParameter(guyMat, "selectedTile", Integer.class),
////                MaterialAccessor.PARAMETER, 7.0f)
////                .target(6.0f)
////                .repeat(-1, 0)
////                .start(tweenManager);
//
//        List<SpriteAnimation> ryuAnimations = new ArrayList<SpriteAnimation>();
//        ryuAnimations.add(new SpriteAnimation("attack", 6, 20, 1.0f));
//        ryuAnimations.add(new SpriteAnimation("idle", 0, 6, 2.5f));
//
//        Sprite ryuSprite = SpriteFactory.fromMaterial("Materials/SimpleSprite/ryu.j3m", assetManager, ryuAnimations);
//
//        Timeline animTimeline = Timeline.createSequence()
//                .push(
//                ryuSprite.play("idle"))
//                .push(
//                ryuSprite.play("idle"))
//                .push(
//                ryuSprite.play("attack"))
//                .push(
//                ryuSprite.play("idle"));
//
//        Timeline.createParallel()
//                .push(animTimeline)
//                .push(Tween.to(geom, SpatialAccessor.LOCAL_TRANSLATION, 1.0f)
//                .target(4.0f, 0.0f, 0.0f).delay(0.3f).ease(TweenEquations.easeOutQuart))
//                .start(tweenManager);
//
//        Texture texture = assetManager.loadTexture("Textures/SimpleSprite/SimpleSprite_2.png");
//        texture.setMinFilter(Texture.MinFilter.NearestNoMipMaps);
//        texture.setMagFilter(Texture.MagFilter.Nearest);
////        
//        //mat2.setTexture("AniTexMap", texture);
//
//        geom2.setMaterial(sprite2);
//
//        geom.setMaterial(ryuSprite.getMaterial());
//        geom.setLocalTranslation(0, 1.0f, 1.0f);
//
////        geom.setLocalScale(0.5f, 1, 1);
//
//        TangentBinormalGenerator.generate(geom);
//        geom.setLocalScale(1.0f);
//
//        rootNode.attachChild(geom);
//        //rootNode.attachChild(geom2);
//    }

}
