/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game.villingen;

import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.debug.Arrow;

/**
 *
 * @author andreas
 */
public class ArrowWrapper {
    Arrow arrow;
    Geometry geometry;
    private Vector3f end;

    public ArrowWrapper(Arrow arrow, Geometry geometry) {
        this.arrow = arrow;
        this.geometry = geometry;
    }

    public Arrow getArrow() {
        return arrow;
    }

    public Geometry getGeometry() {
        return geometry;
    }
    
    public void setStart(Vector3f start) {
        geometry.setLocalTranslation(start);
    }
    
    public void setEnd(Vector3f end) {
        this.end = end;
        arrow.setArrowExtent(end);
    }

    public Vector3f getEnd() {
        return end;
    }

}
