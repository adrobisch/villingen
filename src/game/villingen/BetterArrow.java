/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game.villingen;

import com.jme3.material.Material;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.shape.Cylinder;

/**
 *
 * @author andreas
 */
public class BetterArrow {
    private final Geometry cylinderGeometry;
    private Vector3f startPos;
    private Vector3f endPos;

    public BetterArrow(Node parent, Material mat, Vector3f startPos, Vector3f endPos) {
        this.startPos = startPos;
        this.endPos = endPos;
        
        this.cylinderGeometry = new Geometry("cylinder shape"); // wrap shape into geometry
        cylinderGeometry.setMaterial(mat);                         // assign material to geometry
        
        updateMesh(startPos, endPos);
        
        parent.attachChild(cylinderGeometry); 
    }

    public Geometry getGeometry() {
        return cylinderGeometry;
    }
        
    public void setStart(Vector3f newStart) {
        this.startPos = newStart;
        updateMesh(newStart, endPos);
    }
    
    public void setEnd(Vector3f newEnd) {
        this.endPos = newEnd;
        updateMesh(startPos, newEnd);
    }
    
    private void updateMesh(Vector3f newStart, Vector3f newEnd) {
        float length = newEnd.subtract(newStart).length();
        Vector3f direction = newEnd.subtract(newStart);
        System.out.println("dir"+direction);
        
        Cylinder cyclinder = new Cylinder(10, 10, 0.2f, length, true);
        cylinderGeometry.setMesh(cyclinder);
        
        // For a cone, set the Dome's radialSamples>4 and planes=2.
        
        cylinderGeometry.setLocalTranslation(newStart.add(direction.normalize().mult(length/2)));
        cylinderGeometry.getLocalRotation().lookAt(direction, Vector3f.UNIT_Y);
    }

    public Vector3f getEndPos() {
        return endPos;
    }

    public Vector3f getStartPos() {
        return startPos;
    }
    
    public Vector3f getDirection() {
        return endPos.subtract(startPos);
    }
}
