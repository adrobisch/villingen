package game.villingen;


import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;
import aurelienribon.tweenengine.TweenManager;
import banana.seed.tween.SpatialAccessor;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import game.villingen.VillingenListener.Event;
import java.util.HashSet;
import java.util.Set;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author andreas
 */
public class VillingenNode extends Node {
    Spatial spatial;
    
    Tween moveTimeline;
    float moveSpeed = 0.05f;
    
    boolean isDead = false;
    boolean buildingHit = false;
    
    Set<VillingenListener> listeners = new HashSet<VillingenListener>();
    
    private final TweenManager tweenManager;
    private Vector3f targetDir;
    
    VillingenNode(Spatial spatial, TweenManager tweenManager) {
        this.setName("VillingenNode");
        this.spatial = spatial;
        this.tweenManager = tweenManager;
        
        attachChild(spatial);
    }
    
    public static VillingenNode getVillingenNode(Spatial spatial) {
        Spatial currentParent = spatial.getParent();
        while( currentParent != null ) {
            if (currentParent instanceof VillingenNode) {
                return (VillingenNode) currentParent;
            }
            currentParent = currentParent.getParent();
        }
        return null;
    }
    
    public void handleSelect() {
        System.out.print("SELECT ALTER");
    }

    public Spatial getSpatial() {
        return spatial;
    }
    
 
    @Override
    public String toString() {
        return "VillingenNode ["+spatial+"]";
    }
    
    public Tween tweenTo(Vector3f target) {
        return tweenTo(target.getX(), target.getY(), target.getZ());
    }
    
    public Tween tweenTo(final float x, final float y, final float z) {
        if (moveTimeline != null) {
            moveTimeline.kill();
        }
        
        Vector3f target = new Vector3f(x,y,z);
        targetDir = target.subtract(this.getLocalTranslation());
        
        final Vector3f currentDirection = new Vector3f(targetDir);
        
        moveTimeline = Tween.call(new TweenCallback() {
            public void onEvent(int type, BaseTween<?> source) {
                VillingenNode node = VillingenNode.this;
                final Vector3f currentTranslation = new Vector3f(node.getLocalTranslation());
                node.setLocalTranslation(currentTranslation.add(currentDirection.normalize().mult(moveSpeed)));
                
                if (Math.abs(node.getLocalTranslation().getX()) > Main.KILL_RADIUS ||
                    Math.abs(node.getLocalTranslation().getY()) > Main.KILL_RADIUS ||
                    Math.abs(node.getLocalTranslation().getZ()) > Main.KILL_RADIUS ) {
                    source.kill();
                    node.kill();
                }
                
            }
        }).repeat(Tween.INFINITY, 0.01f).start(tweenManager);
        
        Quaternion targetRotation = new Quaternion(this.getLocalRotation());
        targetRotation.lookAt(targetDir, Vector3f.UNIT_Z);
        
        Tween.to(this, SpatialAccessor.LOCAL_ROTATION_COMP, 0.5f).target(targetRotation.getX(), targetRotation.getY(), targetRotation.getZ(), targetRotation.getW())
                .start(tweenManager);
        
        return null;
    }
    
    public void kill() {
        kill(Event.NODE_KILL);
    }
    
    public void kill(Event event) {
        if (event == Event.NODE_KILL) {
            removeFromParent();
            notifiyListeners(event);
        }
        else if (event == Event.BUILIDING_COLLISION && !buildingHit){
            notifiyListeners(event);
            buildingHit = true;
        }
        else if (!isDead) {
            notifiyListeners(event);
            isDead = true;
        }
    }
    
    public void addListener(VillingenListener listener) {
        listeners.add(listener);
    }
    
    public void notifiyListeners(Event event) {
        for (VillingenListener listener : listeners) {
            listener.notify(event, this);
        }
    }

    public Vector3f getTargetDir() {
        return targetDir;
    }

}
