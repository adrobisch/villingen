/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package banana.seed.tween;

import aurelienribon.tweenengine.TweenAccessor;
import com.jme3.math.Quaternion;
import com.jme3.scene.Spatial;

/**
 *
 * @author andreas
 */
public class SpatialAccessor implements TweenAccessor<Spatial> {
    public static final int LOCAL_TRANSLATION = 1;
    public static final int LOCAL_ROTATION_DEG = 2;
    public static final int LOCAL_SCALE = 3;
    public static final int LOCAL_ROTATION_COMP = 4;

    public SpatialAccessor() {
    }

    public int getValues(Spatial target, int tweenType, float[] returnValues) {
        switch (tweenType) {
            case LOCAL_TRANSLATION:
                returnValues[0] = target.getLocalTranslation().getX();
                returnValues[1] = target.getLocalTranslation().getY();
                returnValues[2] = target.getLocalTranslation().getZ();
                return 3;
            case LOCAL_ROTATION_DEG:
                {float[] angles = target.getLocalRotation().toAngles(null);
                returnValues[0] = (float) Math.toDegrees(angles[0]);
                returnValues[1] = (float) Math.toDegrees(angles[1]);
                returnValues[2] = (float) Math.toDegrees(angles[2]);
                return 3;}
            case LOCAL_ROTATION_COMP:
                returnValues[0] = target.getLocalRotation().getX();
                returnValues[1] = target.getLocalRotation().getY();
                returnValues[2] = target.getLocalRotation().getZ();
                returnValues[3] = target.getLocalRotation().getW();
                return 4;
            case LOCAL_SCALE:
                returnValues[0] = target.getLocalScale().getX();
                returnValues[1] = target.getLocalScale().getY();
                returnValues[2] = target.getLocalScale().getZ();
                return 3;
        }
        return 0;
    }

    public void setValues(Spatial target, int tweenType, float[] newValues) {
        switch (tweenType) {
            case LOCAL_TRANSLATION:
                target.setLocalTranslation(newValues[0], newValues[1], newValues[2]);
                break;
            case LOCAL_ROTATION_DEG:
                // degree to rad
                target.setLocalRotation(new Quaternion().fromAngles((float) Math.toRadians(newValues[0]), (float) Math.toRadians(newValues[1]), (float) Math.toRadians(newValues[2])));
                break;
            case LOCAL_ROTATION_COMP:
                // degree to rad
                float x = newValues[0];
                float y = newValues[1];
                float z = newValues[2];
                float w = newValues[3];
                
                target.setLocalRotation(new Quaternion(x,y,z,w));
                break;
            case LOCAL_SCALE:
                target.setLocalScale(newValues[0], newValues[1], newValues[2]);
                break;
        }
    }
    
}
