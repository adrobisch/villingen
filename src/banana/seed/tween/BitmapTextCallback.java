/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package banana.seed.tween;

import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.TweenCallback;
import com.jme3.font.BitmapText;

/**
 *
 * @author andreas
 */
public class BitmapTextCallback implements TweenCallback {
    
    private final String newText;
    private final BitmapText target;
    
    public BitmapTextCallback(BitmapText target, String newText) {
        this.newText = newText;
        this.target = target;
    }

    public void onEvent(int type, BaseTween<?> source) {
        this.target.setText(newText);
    }
}
