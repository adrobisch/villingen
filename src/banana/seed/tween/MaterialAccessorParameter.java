/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package banana.seed.tween;

import com.jme3.material.Material;

/**
 *
 * @author andreas
 */
public class MaterialAccessorParameter {
    protected Material material;
    protected String materialParameterName;
    protected Class<?> parameterType;

    public MaterialAccessorParameter(Material material, String materialParameterName, Class<?> parameterType) {
        this.material = material;
        this.materialParameterName = materialParameterName;
        this.parameterType = parameterType;
    }

    public Material getMaterial() {
        return material;
    }

    public String getMaterialParameterName() {
        return materialParameterName;
    }

    public Class<?> getParameterType() {
        return parameterType;
    }
    
}
