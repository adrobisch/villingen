/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package banana.seed.tween;

import aurelienribon.tweenengine.TweenAccessor;
import com.jme3.math.ColorRGBA;

/**
 *
 * @author andreas
 */
public class MaterialAccessor implements TweenAccessor<MaterialAccessorParameter> {
    public static final int COLOR = 1;
    public static final int PARAMETER = 2;

    public MaterialAccessor() {
    }

    public int getValues(MaterialAccessorParameter target, int tweenType, float[] returnValues) {
        switch (tweenType) {
            case COLOR:
                ColorRGBA colorVector = (ColorRGBA) target.getMaterial().getParam("Color").getValue();
                returnValues[0] = colorVector.getRed();
                returnValues[1] = colorVector.getGreen();
                returnValues[2] = colorVector.getBlue();
                return 3;
            case PARAMETER:
                Number numberValue = (Number) target.getMaterial().getParam(target.getMaterialParameterName()).getValue();
                if (numberValue instanceof Integer) {
                    returnValues[0] = ((Integer) numberValue).floatValue();
                } else if (numberValue instanceof Float) {
                    returnValues[0] = ((Float) numberValue).floatValue();
                }
                return 1;
        }
        return 0;
    }

    public void setValues(MaterialAccessorParameter target, int tweenType, float[] newValues) {
        switch (tweenType) {
            case COLOR:
                ColorRGBA currentColorVector = (ColorRGBA) target.getMaterial().getParam("Color").getValue();
                ColorRGBA newColorVector = new ColorRGBA(newValues[0], newValues[1], newValues[2], currentColorVector.getAlpha());
                target.getMaterial().setColor("Color", newColorVector);
                break;
            case PARAMETER:
                if (target.getParameterType().isAssignableFrom(Float.class)) {
                    target.getMaterial().getParam(target.getMaterialParameterName()).setValue(((Float) newValues[0]).floatValue());
                } else if (target.getParameterType().isAssignableFrom(Integer.class)) {
                    target.getMaterial().getParam(target.getMaterialParameterName()).setValue((int) newValues[0]);
                }
                break;
        }
    }
    
}
