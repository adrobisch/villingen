/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package banana.seed.sprite;

import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Timeline;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;
import banana.seed.tween.MaterialAccessor;
import banana.seed.tween.MaterialAccessorParameter;
import com.jme3.material.Material;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author andreas
 */
public class Sprite {
    private final Material material;
    
    protected HashMap<String, SpriteAnimation> animations = new HashMap<String, SpriteAnimation>();

    public Sprite(Material material, List<SpriteAnimation> animations) {
        this.material = material;
        this.material.setInt("selectedTile", 0);
        
        for (SpriteAnimation animation :  animations) {
            this.animations.put(animation.getName(), animation);
        }
    }
    
    public Timeline play(String animationKey) {
        final SpriteAnimation animation = animations.get(animationKey);
        
        if (animation == null) {
            throw new IllegalArgumentException("Animation not defined "+animationKey);
        }
        
        return Timeline.createSequence()
                .push(Tween.call(new TweenCallback() {

            public void onEvent(int type, BaseTween<?> source) {
                material.setInt("selectedTile", animation.getStartIndex());
            }
        }))
                .push(
            Tween.to(new MaterialAccessorParameter(material, "selectedTile", Integer.class),
                MaterialAccessor.PARAMETER, animation.getDuration()).target(animation.getEndIndex())
        );
    }

    public Material getMaterial() {
        return material;
    }
    
}
