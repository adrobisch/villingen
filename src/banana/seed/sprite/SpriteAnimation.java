/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package banana.seed.sprite;

/**
 *
 * @author andreas
 */
public class SpriteAnimation {

    String name;
    int startIndex;
    int endIndex;
    float duration;

    public SpriteAnimation(String name, int startIndex, int endIndex, float duration) {
        this.name = name;
        this.startIndex = startIndex;
        this.endIndex = endIndex;
        this.duration = duration;
    }

    public String getName() {
        return name;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public int getEndIndex() {
        return endIndex;
    }

    public float getDuration() {
        return duration;
    }
}
