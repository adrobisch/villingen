/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package banana.seed.sprite;

import com.jme3.asset.AssetManager;
import com.jme3.material.Material;
import java.util.List;

/**
 *
 * @author andreas
 */
public class SpriteFactory {
    public static Sprite fromMaterial(String materialUrl, AssetManager assetManager, List<SpriteAnimation> animations) {
        Material material = assetManager.loadMaterial(materialUrl);
        return new Sprite(material, animations);
    }
}
