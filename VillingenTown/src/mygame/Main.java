package mygame;

import java.util.*;
import com.jme3.app.SimpleApplication;
import com.jme3.asset.TextureKey;
import com.jme3.material.Material;
import com.jme3.math.*;
import com.jme3.renderer.RenderManager;
import com.jme3.bullet.PhysicsSpace;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.collision.shapes.*;
import com.jme3.bullet.control.*;

import com.jme3.input.KeyInput;
import com.jme3.input.MouseInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.controls.MouseButtonTrigger;

import com.jme3.renderer.queue.RenderQueue.ShadowMode;

import com.jme3.scene.Geometry;
import com.jme3.scene.Spatial;
import com.jme3.scene.Node;
import com.jme3.scene.shape.*;
import com.jme3.scene.shape.Sphere.*;

import com.jme3.light.AmbientLight;
import com.jme3.texture.Texture;



/**
 * test
 * @author marcus drobisch
 */
public class Main extends SimpleApplication {

    private BulletAppState bulletAppState;    

            
    private Node floor_node;
    private Node building_node;
    
    private static Sphere bullet;
    private static SphereCollisionShape bulletCollisionShape;
    Material building_mat;
    Material bomb_mat;
    Material mat3;
    
    public static void main(String[] args) {
        Main app = new Main();
        app.start();
    }

    
    private void init_Materials()
    {
        bomb_mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        bomb_mat.setColor("Color", ColorRGBA.Black);

        building_mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        building_mat.setColor("Color", ColorRGBA.Gray);
        
    }
    
    private void add_floor()
    {
        //        
        floor_node = (Node)assetManager.loadModel("Models/streets.scene");
        floor_node.move(0,-32f,0.f);
        floor_node.addLight(new AmbientLight());
        floor_node.addControl(new RigidBodyControl(0));

        Spatial waypoint = floor_node.getChild("Waypoints");
        if(waypoint != null)
            System.out.println("Yeah");
        waypoint.get
        rootNode.attachChild(floor_node);
        this.getPhysicsSpace().add(floor_node);           
    }
    
    private void add_building(Vector3f origin)
    {
        building_node = (Node)assetManager.loadModel("Models/haus.scene");
        building_node.move(origin);
        building_node.setMaterial(building_mat);
        building_node.addLight(new AmbientLight());

        for ( int i = 0; i<building_node.getQuantity() ;i++ )
        {
            building_node.getChild(i).addControl(new RigidBodyControl(9.5f));
            building_node.getChild(i).getControl(RigidBodyControl.class).setFriction(0.6f);
        }
        
        rootNode.attachChild(building_node);

        for ( int i = 0; i<building_node.getQuantity() ;i++ )
        {
            this.getPhysicsSpace().add(building_node.getChild(i));        
        }
                
    }
    
    @Override
    public void simpleInitApp() {
        viewPort.setBackgroundColor(new ColorRGBA(0.3f,0.6f,1.0f,0.8f));
        init_Materials();
        
        bulletAppState = new BulletAppState();
        bulletAppState.setThreadingType(BulletAppState.ThreadingType.PARALLEL);
        stateManager.attach(bulletAppState);
       
        // new Sphere for the bomb
        bullet = new Sphere(32, 32, 0.4f, true, false);
        bulletCollisionShape = new SphereCollisionShape(0.4f);

        add_floor();
        add_building(new Vector3f(0,-30f,0f));
        add_building(new Vector3f(4f,-30f,4f));
        
        inputManager.addMapping("shoot", new MouseButtonTrigger(MouseInput.BUTTON_LEFT));
        inputManager.addListener(actionListener, "shoot");
      
    }
    
    private ActionListener actionListener = new ActionListener() {

        public void onAction(String name, boolean keyPressed, float tpf) {
            if (name.equals("shoot") && !keyPressed) {
                Geometry bulletg = new Geometry("bullet", bullet);
                bulletg.setMaterial(bomb_mat);
                bulletg.setShadowMode(ShadowMode.CastAndReceive);
                bulletg.setLocalTranslation(cam.getLocation());
                
                SphereCollisionShape bulletCollisionShape = new SphereCollisionShape(0.4f);
                RigidBodyControl bulletNode = new BombControl(assetManager, bulletCollisionShape, 1);
//                RigidBodyControl bulletNode = new RigidBodyControl(bulletCollisionShape, 1);
                bulletNode.setLinearVelocity(cam.getDirection().mult(25));
                bulletg.addControl(bulletNode);
                rootNode.attachChild(bulletg);
                getPhysicsSpace().add(bulletNode);
            }
        }
    };    
    
    private PhysicsSpace getPhysicsSpace() {
        return bulletAppState.getPhysicsSpace();
    }    

    @Override
    public void simpleUpdate(float tpf) {
        //TODO: add update code
    }

    @Override
    public void simpleRender(RenderManager rm) {
        //TODO: add render code
    }
}
